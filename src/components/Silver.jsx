import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getSilver } from "../lib/redux/ducks/silverSlice";
import { CircularProgress } from "@material-ui/core";

const Silver = () => {
  const silver = useSelector((state) => state.silver);

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getSilver())
  }, [dispatch]);

  return (
    <div>
      <h1>Silver</h1>
      {silver ? (
        <div>
          <h4>{`Date: ${silver.date}`}</h4>
          <h4>{`GBP: ${(Math.round((silver.gbp) * 100) / 100).toFixed(2)}`}</h4>
          <h4>{`Gram: ${(Math.round((silver.gram) * 100) / 100).toFixed(2)}`}</h4>
        </div>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default Silver;
