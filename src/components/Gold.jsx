import React, { useEffect } from "react";
import { CircularProgress } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { getGold } from "../lib/redux/ducks/goldSlice";

const Gold = () => {
  const gold = useSelector((state) => state.gold);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getGold());
  }, [dispatch]);
  
  return (
    <div>
      <h1>Gold</h1>
      {gold && gold.eur && gold.gbp && gold.usd ? (
            <div>
              <h2>{`Date: ${gold.date}`}</h2>
              <h4>{`USD - AM: ${gold.usd.am} PM:${gold.usd.pm}`}</h4>
              <h4>{`GBP - AM: ${gold.gbp.am} PM:${gold.gbp.pm}`}</h4>
              <h4>{`EUR - AM: ${gold.eur.am} PM:${gold.eur.pm}`}</h4>
            </div>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default Gold;
