import { createSlice } from "@reduxjs/toolkit";

export const goldSlice = createSlice({
  name: "gold",
  initialState: {},

  reducers: {
    getGold() {},
    setGold(state, action) {
      const golddata = action.payload;
      return { ...state, ...golddata };
    },
  },
});

export const { getGold, setGold } = goldSlice.actions;

export default goldSlice.reducer;
