import { createSlice } from "@reduxjs/toolkit";

export const silverSlice = createSlice({
  name: "silver",
  initialState: {},

  reducers: {
    getSilver() {},
    setSilver(state, action) {
      const silverdata = action.payload;
      return { ...state, ...silverdata };
    },
  },
});

export const { getSilver, setSilver } = silverSlice.actions;

export default silverSlice.reducer;