import { takeLatest } from "redux-saga/effects";
import { getGold } from "../ducks/goldSlice";
import { getSilver } from "../ducks/silverSlice";
import { handlersGetGold } from "./handlers/gold";
import { handlerGetSilver } from "./handlers/silver";

export function* watcherSaga(){
    yield takeLatest( getGold.type, handlersGetGold )
    yield takeLatest( getSilver.type, handlerGetSilver )
    
}