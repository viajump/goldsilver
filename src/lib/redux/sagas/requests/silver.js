import axios from "axios";

export function requestGetSilver() {
  return axios.request({
    method: "GET",
    url: "https://api.uloom.com/api/silver/",
  });
}