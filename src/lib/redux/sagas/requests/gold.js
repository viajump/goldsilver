import axios from "axios";

export function requestGetGold() {
  return axios.request({
    method: "GET",
    url: "https://api.uloom.com/api/gold",
  });
}