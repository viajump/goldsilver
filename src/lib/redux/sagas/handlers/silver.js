import { call, put } from "redux-saga/effects";
import { setSilver } from "../../ducks/silverSlice";
import { requestGetSilver } from "../requests/silver";

export function* handlerGetSilver() {
  try {
    const response = yield call(requestGetSilver);
    const { data } = response;
    yield put(setSilver({ ...data }));
  } catch (error) {
    console.log(error);
  }
}
