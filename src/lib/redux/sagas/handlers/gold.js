import { call, put } from "redux-saga/effects";
import { setGold } from "../../ducks/goldSlice";
import { requestGetGold } from "../requests/gold";

export function* handlersGetGold() {
  try {
    const response = yield call(requestGetGold);
    const { data } = response;
    yield put(setGold( ...data ));
  } catch (error) {
    console.log(error);
  }
}
