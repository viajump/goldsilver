import Gold from "./components/Gold";
import Silver from "./components/Silver";
import "./App.css";

function App() {
  return (
    <div className="App">
      <Gold />
      <Silver />
    </div>
  );
}

export default App;
